from plotly import tools
from utils import *

import plotly.offline as py
import plotly.graph_objs as go

nb_divide = 5
ff = ["Flying","Astronomy"]
# ff = ["Arithmancy","Astronomy","Herbology","Defense Against the Dark Arts","Divination"]
ff = ["Herbology","Defense Against the Dark Arts","Divination","Muggle Studies","Transfiguration","Care of Magical Creatures","Charms","Flying"]
# ff = ["Arithmancy","Astronomy","Herbology","Defense Against the Dark Arts","Divination","Muggle Studies","Ancient Runes","History of Magic","Transfiguration","Potions","Care of Magical Creatures","Charms","Flying"]
# ff=  ['Herbology', 'Defense Against the Dark Arts']
sqr = int(len(ff))

def add_figure(arr, fig, pos):
    data = []
    layout = go.Layout(barmode='stack')
    color = {
        'Gryffindor' : 'red',
        'Ravenclaw' : 'blue',
        'Hufflepuff' : 'green',
        'Slytherin' : 'cyan'
    }
    print 1 + (pos/sqr), 1 + (pos%sqr)
    for h in arr:
        (xxx, yyy) = arr[h]
        fig.append_trace({'x': xxx, 'y': yyy,'type': 'scatter',
                          'name': h, 'text': h, 'opacity': 0.6, 'mode' : 'markers','showlegend':True if pos == 0 else False, 'marker': {'size': 8 - nb_divide, 'color' : color[h]}}, 1 + (pos/sqr), 1 + (pos%sqr))

def draw(ff):
    global sqr
    sqr = int(len(ff))
    fig = tools.make_subplots(rows=sqr, cols=sqr, subplot_titles=(ff))
    dicc = {}
    i = 0

    print 1 + (i/sqr), 1 + (i%sqr)
    for featurex in ff:
        for feature in ff:
            for m in ['Gryffindor', 'Ravenclaw', 'Hufflepuff', 'Slytherin']:
                dicc[m] = get_safe_normalize_array_pair(feature, featurex, m, 0.2)
            add_figure(dicc, fig, i)
            i += 1
    fig['layout'].update(title='Repartitions des notes de poudlard par maison')
    py.plot(fig, filename='Poudlard houses')

if __name__ == '__main__':
    if len(sys.argv) == 2:
        read_csv(sys.argv[1])
        draw(ff)
    else:
        print ("Usage : python2.7 describe.py [dataset].csv")
