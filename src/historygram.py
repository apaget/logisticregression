from plotly import tools
from utils import *

import plotly.offline as py
import plotly.graph_objs as go

nb_divide = 60
sqr = 4

def add_figure(arr, fig, pos):
    data = []
    layout = go.Layout(barmode='stack')
    color = {
        'Gryffindor' : 'red',
        'Ravenclaw' : 'blue',
        'Hufflepuff' : 'green',
        'Slytherin' : 'cyan'
    }
    print 1 + (pos/sqr), 1 + (pos%sqr)
    for h in arr:
        fig.append_trace({'x': [float(i)/nb_divide for i in range(nb_divide)], 'y': arr[h], 'type': 'bar',
                          'name': h, 'text': h, 'opacity': 0.6, 'showlegend':True if pos == 0 else False, 'marker': {'color' : color[h]}}, 1 + (pos/sqr), 1 + (pos%sqr))

def draw(feature):
    ff = ["Arithmancy","Astronomy","Herbology","Defense Against the Dark Arts","Divination","Muggle Studies","Ancient Runes","History of Magic","Transfiguration","Potions","Care of Magical Creatures","Charms","Flying"]
    fig = tools.make_subplots(rows=sqr, cols=sqr, subplot_titles=(ff))
    dicc = {}
    i = 0

    print 1 + (i/sqr), 1 + (i%sqr)
    for feature in ff:
        for m in ['Gryffindor', 'Ravenclaw', 'Hufflepuff', 'Slytherin']:
            dicc[m] = get_historigram(feature, m)
        add_figure(dicc, fig, i)
        i += 1
    fig['layout'].update(title='Repartitions des notes de poudlard par maison')
    py.plot(fig, filename='Poudlard houses')

def get_historigram(f, house):
    final = [0 for _ in range(nb_divide)]
    arr = get_safe_normalize_array(f, house, 'Hogwarts House')
    for x in arr:
        i = 0
        while (i/float(nb_divide)) < x:
            i += 1
        final[i - 1] += 1
    return final

if __name__ == '__main__':
    if len(sys.argv) == 2:
        read_csv(sys.argv[1])
        draw('Divination')
    else:
        print ("Usage : python2.7 describe.py [dataset].csv")
