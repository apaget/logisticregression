import sys
import math
import pandas
import pdb
import random

dic = {}
features = []

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

def map(x, in_min, in_max, out_min, out_max):
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;

class Value(object):
    def __init__(self, str_val):
        self.str = str_val
        self.isSet = True
        self.isNb = True
        if str_val == "":
            self.val = 0
            self.isSet = False
            self.isNb = False
        else:
            # if str_val == "nan":
                # pdb.set_trace()
            if str_val != "Nan" and isfloat(str_val) and isfloat(str_val) != float("Nan"):
                self.val = float(str_val)
            else:
                self.val = str_val
                self.isNb = False
                # print str_val

class Feature:
    def __init__(self, name):
        self.name = name
        self.type = "none"
        self.v = []
        self.d = {}
        self.d["Count"] = 0
        self.d["Mean"] = 0
        self.d["Std"] = 0
        self.d["Min"] = sys.float_info.max
        self.d["25%"] = 0
        self.d["50%"] = 0
        self.d["75%"] = 0
        self.d["Max"] = -9999999.0



    def get_array(self):
        arr = []
        for elem in self.v:
            arr.append(elem.val)
        return arr

    def get_safe_array(self):
        arr = []
        for elem in self.v:
            if elem.isSet and elem.isNb:
                arr.append(elem.val)
            else:
                arr.append(0)
        return arr

    def get_quartil(self, n):
        if self.type == "num":
            v = []
            for i in self.v:
                if i.isNb:
                    v.append(i.val)
            s = sorted(v, reverse=False)
            i = 0
            k = (len(s) - 1) * n
            fff = math.floor(k)
            c = math.ceil(k)
            if fff == c:
                return s[int(k)]
            else:
                return (s[int(fff)] * (c - k)) + (s[int(c)] * (k-fff))

    def get_std(self):
        if self.type == "num":
            v = []
            for i in self.v:
                if i.isNb:
                    v.append(i.val)
            sqrmean = 0
            for i in v:
                sqrmean += abs((i - self.d['Mean']))**2
            return math.sqrt(sqrmean/float(len(v) - 1))

    def post_calc(self):
        self.calc()
        if self.type == "num":
            self.d['25%'] = self.get_quartil(0.25)
            self.d['50%'] = self.get_quartil(0.50)
            self.d['75%'] = self.get_quartil(0.75)
            self.d['Std'] = self.get_std()

    def calc(self):
        avg = 0
        for e in self.v:
            if e.isSet:
                self.d["Count"] += 1
            if e.isNb:
                avg += e.val
                if e.val < self.d["Min"]:
                    self.d["Min"] = e.val
                if e.val > self.d["Max"]:
                    self.d["Max"] = e.val
        if float(self.d["Count"]):
            self.d['Mean'] = avg / float(self.d["Count"])

def read_line(line):
    i = 0
    arr = line.split(",")
    if len(dic.keys()) != len(arr):
        print ("Le csv n'est pas correctement formatter")
        sys.exit(1)
    for f in features:
        dic[f].v.append(Value(arr[i]))
        if len(dic[f].v) > 1:
            if dic[f].v[-1].isSet and dic[f].v[-2].isSet and dic[f].v[-1].isNb != dic[f].v[-2].isNb and not dic[f].v[-1].isNb and not dic[f].v[-2].isNb:
                print ("Le csv ne contient pas les meme type de donner")
                sys.exit(1)
            else:
                if dic[f].v[-1].isSet == False:
                    dic[f].type = "str"
                elif dic[f].v[-1].isNb:
                    dic[f].type = "num"
                else:
                    dic[f].type = "str"
        i += 1

def get_safe_normalize_array(feature, house, F):
    arr = []
    houses = []
    features = []
    for xx in range(len(dic[feature].v)):
        if dic[feature].v[xx].isSet and dic[feature].v[xx].isNb:
            houses.append(dic[F].v[xx].val)
            features.append(dic[feature].v[xx].val)
    if len(houses) != len(features):
        print "Len diff : {} {}".format(len(houses), len(features))
        return
    for i in range(len(houses)):
        if house == 'all':
            arr.append(map(features[i], dic[feature].d['Min'], dic[feature].d['Max'], 0, 1))
        elif houses[i] == house:
            arr.append(map(features[i], dic[feature].d['Min'], dic[feature].d['Max'], 0, 1))
    return arr


def get_safe_normalize_array_pair(feature, feature2, house, freq):
    arr = []
    arr2 = []
    houses = []
    features = []
    features2 = []
    for xx in range(len(dic[feature].v)):
        if dic[feature].v[xx].isSet and dic[feature].v[xx].isNb:
            houses.append(dic['Hogwarts House'].v[xx].val)
            features.append(dic[feature].v[xx].val)
            features2.append(dic[feature2].v[xx].val)
    if len(houses) != len(features):
        print "Len diff : {} {}".format(len(houses), len(features))
        return
    for i in range(len(houses)):
        if house == 'all':
            arr.append(map(features[i], dic[feature].d['Min'], dic[feature].d['Max'], 0, 1))
        elif houses[i] == house:
            if random.random() < freq:
                arr.append(map(features[i], dic[feature].d['Min'], dic[feature].d['Max'], 0, 1))
                arr2.append(map(features2[i], dic[feature2].d['Min'], dic[feature2].d['Max'], 0, 1))
    return arr, arr2

def check_f(features, i):
    for f in features:
        if not dic[f].v[i].isSet:
            return False
    return True

def get_safe_normalize_arrays(features, freq, house):
    arr = []
    y = []
    # for i, f in enumerate(dic['Hogwarts House'].v):
        # arr.append([])
    rd = 0
    for i, v in enumerate(dic['Hogwarts House'].v):
        if random.random() < freq and check_f(features, i):
            arr.append([])
            # arr.append([])
            for ii, f in enumerate(features):
                # print i, arr
                arr[rd].append(map(dic[f].v[i].val, dic[f].d['Min'], dic[f].d['Max'], 0, 1))
            if house == v.val:
                y.append(1)
            else:
                y.append(0)
            rd +=1
    return arr, y



def get_safe_normalize_array_3d(feature, feature2, feature3, house, freq):
    arr = []
    arr2 = []
    arr3 = []
    houses = []
    features = []
    features2 = []
    features3 = []
    for xx in range(len(dic[feature].v)):
        if dic[feature].v[xx].isSet and dic[feature].v[xx].isNb:
            houses.append(dic['Hogwarts House'].v[xx].val)
            features.append(dic[feature].v[xx].val)
            features2.append(dic[feature2].v[xx].val)
            features3.append(dic[feature3].v[xx].val)
    if len(houses) != len(features):
        print "Len diff : {} {}".format(len(houses), len(features))
        return
    for i in range(len(houses)):
        if house == 'all':
            arr.append(map(features[i], dic[feature].d['Min'], dic[feature].d['Max'], 0, 1))
        elif houses[i] == house:
            if random.random() < freq:
                arr.append(map(features[i], dic[feature].d['Min'], dic[feature].d['Max'], 0, 1))
                arr2.append(map(features2[i], dic[feature2].d['Min'], dic[feature2].d['Max'], 0, 1))
                arr3.append(map(features3[i], dic[feature3].d['Min'], dic[feature3].d['Max'], 0, 1))
    return (arr, arr2, arr3)

def post_calc():
    for tag in dic.keys():
        dic[tag].post_calc()

def register_head(head):
    global features
    for tag in head.split(","):
        features.append(tag.strip())
        dic[tag.strip()] = Feature(tag.strip())

def read_csv(name):
    try:
        with open(name) as file:
            f = iter(file)
            try:
                head = next(f)
            except Exception as e:
                print ("Le fichier semble etrange")
                sys.exit(1)
            if head:
                register_head(head)
            for line in f:
                read_line(line)
            post_calc()
    except Exception as e:
        print ("Le fichier na pas ete trouver")
        sys.exit(1)
