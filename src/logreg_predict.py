import sys
import math
import pandas
import pdb
import random
from logreg_train import *

trainers = {}

def read_line_theta(line):
    global trainers
    i = 0
    arr = line.split(",")
    if 7 != len(arr) or arr[0] not in ['Ravenclaw', 'Gryffindor', 'Slytherin', 'Hufflepuff']:
        print ("Le csv n'est pas correctement formatter")
        sys.exit(1)
    trainers[arr[0]] = LogTrain(5, 0.0, arr[0])
    trainers[arr[0]].theta = [float(arr[i]) for i in range(1, 6)]
    trainers[arr[0]].bias = float(arr[-1])
    # print arr[0], trainers[arr[0]].theta, trainers[arr[0]].bias

def read_theta(name):
    try:
        with open(name) as file:
            f = iter(file)
            try:
                for line in f:
                    # print line
                    read_line_theta(line)
            except Exception as e:
                print ("Le fichier semble etrange", e)
                sys.exit(1)
            # post_calc()
    except Exception as e:
        print ("Le fichier na pas ete trouver")
        sys.exit(1)


def get_prediction_final():
    score = {}
    win = 0
    tp = 0
    fp = 0
    arr = []
    for k in models:
        score[k] = 0.0
    for i, m in enumerate(dic['Hogwarts House'].v):
        for k in score:
            x = []
            for kk in models[k]:
                x.append(map(dic[kk].v[i].val, dic[kk].d['Min'], dic[kk].d['Max'], 0.0, 1.0))
            # print x
            score[k] = trainers[k].pred_value(x)
        if m.isSet:
            # print m.val, get_best_possibility(score))
            arr.append(m.val == get_best_possibility(score))
        else:
            arr.append(get_best_possibility(score))
    return arr

def print_in_file(f):
    file = "Index,Hogwarts House\n"
    for i, m in enumerate(f):
        file += ",".join([str(i), m]) + '\n'
    with open('house.csv', 'wb') as houses:
        houses.write(file)
        houses.close()

if __name__ == '__main__':
    # print models
    nb_win = 0
    if len(sys.argv) == 3:
        read_theta(sys.argv[1])
        read_csv(sys.argv[2])
        final = get_prediction_final()
        if not isinstance(final[0], bool):
            try:
                print_in_file(final)
            except Exception as e:
                print e
        else:
            for i, v in enumerate(final):
                # print final
                if v:
                    nb_win += 1
            print "Accuraty : %.2f%%" % (100 * nb_win / float(len(final)))



        # print dic['']
