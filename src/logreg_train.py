from utils import *
from sklearn.linear_model import LogisticRegression
import numpy as np
import threading
import time
from multiprocessing.dummy import Pool as ThreadPool
import random
from sklearn.utils import shuffle

killThread = False
trainers = {}
score = {}
nb_iter = 0
max_iter = 500

models = {  'Ravenclaw' : ['Defense Against the Dark Arts', 'Herbology', 'Flying', 'Charms', 'Divination'],
            'Gryffindor' : ['Defense Against the Dark Arts', 'Herbology', 'Flying', 'Charms', 'Divination'],
            'Hufflepuff' : ['Defense Against the Dark Arts', 'Herbology', 'Flying', 'Charms', 'Divination'],
            'Slytherin' :['Defense Against the Dark Arts', 'Herbology', 'Flying', 'Charms', 'Divination']
            }

# ff=  ['Flying', 'Astronomy']
# ff = ["Arithmancy","Astronomy","Herbology","Defense Against the Dark Arts","Divination","Muggle Studies","Ancient Runes","History of Magic","Transfiguration","Potions","Care of Magical Creatures","Charms","Flying"]
# ffs=  ['Divination', 'Herbology']
# models = {  'Ravenclaw' : ff,
#             'Gryffindor' : ff,
#             'Hufflepuff' : ff,
#             'Slytherin' : ff
#             }

class LogTrain(object):
    """docstring for LogTrain."""
    def __init__(self, dim, lr, name):
        self.bias = 0.0
        self.theta = [0.0 for _ in range(dim)]
        self.dim = dim
        self.name = name
        self.learning_rate = lr
        self.last_err = sys.float_info.max
        self.last_win = 0.0

    def h(self, x, i):
        return self.g((x[i] * self.theta[i]) + self.bias)

    def hh(self, x):
        sum = 0.0
        for i, xx in enumerate(x):
            # print xx, i
            # print self.theta
            sum += (x[i] * self.theta[i])
        return self.g(sum + self.bias)


    def g(self, z):
        return 1/(1 + math.exp(-z))



    def np_sig(self, Z):
        return 1 / (1 + np.e**(-Z))

    def deriv(self, arr_x, arr_y):
        sum = [0.0 for _ in range(self.dim)]
        dz = [0.0 for _ in range(self.dim)]
        dws = [0.0 for _ in range(self.dim)]
        dzs = 0.0
        for elem, y in zip(arr_x, arr_y):
            A = self.hh(elem)
            dz = A - y
            dzs += dz
            for i, xx in enumerate(elem):
                dws[i] += elem[i] * dz
        for i, d in enumerate(dws):
            dws[i] = dws[i] / 1.0#float(len(arr_x))
        return dws, dzs

    def loose(self, xx, yy):
        sum = 0.0
        for i, e in enumerate(xx):
            sum += yy[i] * math.log(self.hh(e)) + (1 - yy[i]) * math.log(1 - self.hh(e))
        return -(sum/float(len(xx)))

    def iter(self, max, xx, yy):
        _ = 0
        global nb_iter
        while _ < max and not killThread:
            sum = 0.0
            dz = 0.0
            d, dz = self.deriv(xx, yy)
            for i, item in enumerate(d):
                self.theta[i] -= self.learning_rate * d[i]
            self.bias -= dz * self.learning_rate
            if _ % 10 == 0:
                self.last_win = self.get_percent_win(xx, yy)
                self.last_err = self.loose(xx, yy)
                score[self.name] = self.last_win
            _ += 1
            nb_iter += 1

    def pred_value(self, x):
        sol = self.np_sig(np.matmul(x, np.array(self.theta)) + np.array(self.bias))
        return sol

    def pred(self, x):
        sol = self.np_sig(np.matmul(x, np.array(self.theta)) + np.array(self.bias))
        return sol
        if sol > 0.5:
            return True
        return False
        tp = 0.0
        for i, xx in enumerate(x):
            if self.h(x, i) > 0.5:
                return True
        return False

    def get_percent_win(self, xx, yy):
        win = 0.0
        tp = 0
        fp = 0
        for i, x in enumerate(xx):
            if (self.pred(x) > 0.5 and yy[i] == 1):
                win += 1
                tp += 1
            if (self.pred(x) > 0.5 and yy[i] == 0):
                fp += 1
        if tp + fp > 0:
            return tp/float(tp + fp)
        else:
            return 0.0

def train_model(k):
    print "Train model for {}".format(k)
    trainers[k] = LogTrain(int(len(models[k])), 6e-4, k)
    x, y = get_safe_normalize_arrays(models[k], 0.5, k)
    trainers[k].iter(max_iter, x, y)

def write_ui():
    global killThread
    while threading.active_count() > 2:
        try:
            if len(score) == 4:
                print "Gryffindor : %4.4f, Slytherin : %4.4f, Hufflepuff: %4.4f, Ravenclaw : %4.4f : %.2f %%" % (score['Gryffindor'], score['Slytherin'], score['Hufflepuff'], score['Ravenclaw'], (100*nb_iter)/float(max_iter * 4))
            time.sleep(1.0)
        except KeyboardInterrupt:
            print "Ctrl-c received! Sending kill to threads..."
            killThread = True

def launch_multiThread():
    threads = []
    for i, k in enumerate(models):
        threads.append(threading.Thread(target=train_model, args=(k,)))
        threads[i].daemon = True
        threads[i].start()
    write_ui()
    write_score()
    print "\n\nTotal accuraty : %.2f" % get_prediction([''])

def get_best_possibility(arr):
    max = sys.float_info.min
    house = 'none'
    for k in arr:
        if arr[k] > max:
            max = arr[k]
            house = k
    return house


def get_prediction(features):
    score = {}
    win = 0
    tp = 0
    fp = 0
    for k in models:
        score[k] = 0.0
    for i, m in enumerate(dic['Hogwarts House'].v):
        for k in score:
            x = []
            if m.val in score:
                for kk in models[k]:
                    x.append(map(dic[kk].v[i].val, dic[kk].d['Min'], dic[kk].d['Max'], 0.0, 1.0))
                score[k] = trainers[k].pred_value(x)
        if get_best_possibility(score) == m.val:
            win += 1
            tp += 1
        else:
            fp += 1
    win *= 100
    win = win/float(len(dic['Hogwarts House'].v))
    return win


def write_score():
    file = ""
    for k in trainers:
        tmp = []
        tmp.append(k)
        for t in trainers[k].theta:
            tmp.append(str(t))
        tmp.append(str(trainers[k].bias))
        print ','.join(tmp);
        file += ','.join(tmp)
        file += "\n"
    with open("weight", 'w') as f:
        f.write(file)

if __name__ == '__main__':
    if len(sys.argv) >= 2:
        read_csv(sys.argv[1])
        ff = ['Charms']
        arr = []
        launch_multiThread()
        sys.exit(1)
        for i in range(5):
            train_models(ff)
            arr.append(get_prediction(ff))
            write_score()

        print arr
    else:
        print ("Usage : python2.7 describe.py [dataset].csv")
