import sys
import math
import pandas
import pdb
from utils import *


def print_describe():
    tab     = [[""]]
    str     = ""
    i       = 0
    maxs    = []

    print "%-10.11s" % "",
    for f in features:
        if dic[f].type == "num" and dic[f].name.strip() != "":
            tab.append([dic[f].name])
    for k in dic[dic.keys()[0]].d:
        tab[i].append(k)

    for f in features:
        if dic[f].type == "num":
            i+=1
            for k in dic[f].d:
                tab[i].append(dic[f].d[k])

    for l in tab:
        max = 0
        for ll in l:
            if isinstance(ll, int) or isinstance(ll, float):
                if ll and len("%f" % ll) > max:
                    max = len("%f" % ll)
            else:
                if ll and len("{}".format(ll)) > max:
                    max = len("{}".format(ll))
        maxs.append(max + 1)
    i = 1
    for f in features:
        if dic[f].type == "num":
            print "%{}s".format(maxs[i]) % (dic[f].name),
            i+=1
    print ""
    for k in dic[dic.keys()[0]].d:
        print "%-10s" % k,
        i = 1
        for f in features:
            if dic[f].type == "num":
                # tab[i].append(dic[f].d[k])
                print "%{}f".format(maxs[i]) % (dic[f].d[k]),
                i += 1
        print "\n",

if __name__ == '__main__':
    if len(sys.argv) == 2:
        read_csv(sys.argv[1])
        print_describe()
    else:
        print ("Usage : python2.7 describe.py [dataset].csv")
