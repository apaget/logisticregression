# LogisticRegression

The goal of this project was for education only. With the data available,
I've made an algorithm to predict the student's houses of poudlard

Usage
-----

To discover the data use 

```
python src/describe.py data/dataset_train.csv
python src/historygram.py data/dataset_train.csv
python src/pair_plot.py data/dataset_train.csv
python src/scatter.py data/dataset_train.csv
```

To make the Ai learn run 

```
python src/logreg_train.py data/dataset_train.csv
```

This command will create the weigth of the network (file 'weigth') witch we can use to predict the others student's house with 

```
python src/logreg_predict.py weight data/dataset_test.csv
```
Result
------
We can see the result in house.csv file.
